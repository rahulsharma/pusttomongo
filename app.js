var fs = require('fs'),
    url = require('url'),

    mongo = require('mongodb'),
    QueryCommand = mongo.QueryCommand,
    Cursor = mongo.Cursor,
    Collection = mongo.Collection;

var uristring = "mongodb://localhost/vizAnalytics"
var mongoUrl = url.parse(uristring);

//
// Open mongo database connection
// A capped collection is needed to use tailable cursors
//
mongo.Db.connect(uristring, function (err, db) {
    console.log("Attempting connection to " + mongoUrl.protocol + "//" + mongoUrl.hostname + " (complete URL supressed).");
    db.collectionNames('varImpactConfig', function (err, colls) {
        if (colls.length > 0) {
            console.log('Attempting to drop old "varImpactConfig" collection.')
            db.dropCollection("varImpactConfig", function (err) {
                if (err) {
                    console.log("Error dropping collection: ", err);
                    process.exit(2);
                } else {
                    createAndPopulate(db);
                }
            });
        } else {
            createAndPopulate(db);
        }
    });
});

function createAndPopulate(db) {
    db.createCollection("varImpactConfig", {capped: true, size: 8000000},
        function (err, collection) {
            if (err) {
                console.log("Error creating collection: ", err);
                process.exit(3);
            }

            console.log("Success connecting to " + mongoUrl.protocol + "//" + mongoUrl.hostname + ".");

            insertDocs(collection);
        });
}

insert = 1;

function insertDocs(collection) {
    var doc = docs[0];

    var coefficient = randomIntFromInterval(-100, 100);
    var univariateCoefficient = randomIntFromInterval(-100, 100);
    var alteredEffect = randomIntFromInterval(0, 100);

    doc.ModelResults[0].dependentVariables[0].independentVariables[0].coefficient = coefficient;
    doc.ModelResults[0].dependentVariables[0].independentVariables[0].univariateCoefficient = univariateCoefficient;
    doc.ModelResults[0].dependentVariables[0].independentVariables[0].alteredEffect = alteredEffect;

    if (doc["_id"]) delete doc["_id"];
    time = new Date()
    doc.time = time.getTime()
    doc.ordinal = insert;

    if (insert == Math.pow(2, 30) - 1)
        insert = 1;
    else
        insert = insert + 1;

    collection.insert(doc);

    setTimeout(insertDocs, 1000, collection)
}

function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min).toFixed(8);
}

docs = [
    {
        "ModelResults": [
            {
                "segment": "Buildings",
                "dependentVariables": [
                    {
                        "name": "profit",
                        "diagnostics": [
                            {
                                "name": "rSquared",
                                "value": 0.2560193363854041
                            },
                            {
                                "name": "adjRsquared",
                                "value": 0.2510498469279483
                            },
                            {
                                "name": "sumSquares",
                                "value": 62000.364764670696
                            },
                            {
                                "name": "residualSumSquares",
                                "value": 46127.072521966715
                            },
                            {
                                "name": "errorVar",
                                "value": 9.93903738891763
                            }
                        ],
                        "independentVariables": [
                            {
                                "univariatePValue": 2.078063719222756e-12,
                                "name": "gainprojs",
                                "tValue": 9.142068526147996,
                                "coefficient": 49.57861637618857,
                                "univariateCoefficient": 9.25699281706172,
                                "univariateStdErr": 0.8511416771462093,
                                "stdError": 5.423128937874904,
                                "alteredEffect": 7.640818315837822,
                                "pValue": 9.705067780386302e-11,
                                "univariateTValue": 10.875971727879039
                            }
                        ]
                    }
                ]
            }
        ]
    }
];



